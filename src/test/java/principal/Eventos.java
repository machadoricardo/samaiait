package principal;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import static principal.DriverFactory.getDriver;

public abstract class Eventos {
	
	protected void escrever(By by, String texto) {
		getDriver().findElement(by).clear();
		getDriver().findElement(by).sendKeys(texto);
	}

	protected void escreverPorName(String id_campo, String texto) {
		escrever(By.name(id_campo), texto);
	}

	protected void escrever(String id_campo, String texto) {
		escrever(By.id(id_campo), texto);
	}

	protected String obterTexto(By by) {
		return getDriver().findElement(by).getText();
	}

	protected void clicar(By by) {
		getDriver().findElement(by).click();
	}

	protected void clicarEnviarFormulario(By by) {
		getDriver().findElement(by).submit();
	}

	protected void esperar(int tempo, TimeUnit timeUnit) {
		getDriver().manage().timeouts().implicitlyWait(tempo, timeUnit);
	}

	protected void novaPagina() {
		for (String winHandle : getDriver().getWindowHandles()) {
			getDriver().switchTo().window(winHandle);
		}
	}

}
