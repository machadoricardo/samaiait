package principal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import static principal.DriverFactory.getDriver;
import static principal.DriverFactory.killDriver;

import java.util.concurrent.TimeUnit;


public class PrincipalTest extends Eventos {

	public WebDriverWait wait;
	
	@Before
	public void init() {
		System.setProperty("webdriver.gecko.driver", "src/test/resources/geckodriver");
		wait = new WebDriverWait(getDriver(), 20);
	}

	/**
	 * Contexto: eu como usuário quero buscar no Google pela palavra samaia
	 * encontrar o resultado (site da samaia it)
	 * clicar no link e acessar o site
	 * clicar em saiba mais e acessar o site portal investigativo
	 * acessar o formulário de contato e preenche-lo com minhas informações e enviar 
	 * o sistema irá mostrar na tela mensagem de sucesso ('Obrigado por contatar a Samaia IT! ....')
	 * @author ricardo.machado
	 */
	@Test
	public void entreEmContatoSucesso() {
		buscarGoogle();
		clicarSaibaMais();
		wait.until(ExpectedConditions.not(ExpectedConditions.invisibilityOfElementLocated(By.id("advancedButton"))));
		resolverCertificadoSeguranca();
		preencherFormulario();
		System.out.println(obterTexto(By.xpath("//div[@id='contato_lvl01']")));
		wait.until(ExpectedConditions
				.not(ExpectedConditions.attributeToBe(By.xpath("//table[1]/tbody[1]/tr[1]/td[1]"), "width", "50%")));
		validarRetorno();
	}

	@After
	public void finalize() {
		killDriver();
	}

	private void clicarSaibaMais() {
		esperar(5, TimeUnit.SECONDS);
		clicar(By.xpath("//span[contains(.,'SAIBA MAIS')]"));
		novaPagina();
	}

	private void resolverCertificadoSeguranca() {
		getDriver().findElement(By.id("advancedButton")).click();
		esperar(30, TimeUnit.SECONDS);
		getDriver().findElement(By.id("exceptionDialogButton")).click();
	}

	private void buscarGoogle() {
		getDriver().get("http://www.google.com.br");
		clicar(By.name("q"));
		escreverPorName("q", "samaia");
		clicarEnviarFormulario(By.name("q"));
		esperar(5, TimeUnit.SECONDS);
		clicar(By.xpath("//a[@href='https://samaiait.com.br/']"));
	}

	private void validarRetorno() {
		boolean existeMensagem = obterTexto(By.xpath("//h1[contains(.,'Obrigado por contatar a Samaia IT!')]"))
				.contains("Obrigado por contatar");
		Assert.assertTrue(existeMensagem);
	}

	private void preencherFormulario() {
		esperar(15, TimeUnit.SECONDS);
		getDriver().findElement(By.xpath("//a[contains(.,'Contato')]")).click();
		escrever("nome", "João da Silva");
		escrever("email", "joao@email.com");
		escrever("telefone", "(48)99995555");
		escrever("mensagem", "Mensagem para teste automatizado. Favor desconsiderá-la.");
		clicarEnviarFormulario(By.xpath("//input[contains(@name,'enviar')]"));
	}



}

